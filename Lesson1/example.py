# # Hillel school
# # Вариант 1
# first = 10
# second = 30

# # sol - (Solution - решение)
# sol = first + second
# print(sol)
# print(type(sol))
#
# sol = first - second
# print(sol)
#
# sol = first * second
# print(sol)
#
# sol = first / second
# print(sol)
# print(type(sol))
#
# sol = first // second
# print(sol)
#
# sol = first % second
# print(sol)
# sol = first  second
# print(sol)
#
# first  = first +  2
# first +=2
# first-=5
# print(first >0)
# print(first <0)
#
#
# print("\u2028")  # Разделитель строк (сделел чтобы читабельно было 2 примера при выводе)
#
# # Как я это вижу
# # Вариант  второй ЛЕНИВЫЙ
#
# first = 10
# second = 30
# print(first + second)
# print(first - second)
# print(first * second)
# print(first / second)
# print(first // second)
# print(first % second)
# print(first  second)


# Numbers
my_int = 44
print(my_int)
print(type(my_int))
print("\u2028")

res = my_int + 1
print(res)
print(type(res))
print("\u2028")

res = my_int - 3
print(res)
print(type(res))
print("\u2028")

res = my_int * 4
print(res)
print(type(res))
print("\u2028")

res = '12345'
res = my_int / 2
print(type(res))
print(res)
print("\u2028")

res = my_int
3  # возведение в степень
print(type(res))
print(res)
print("\u2028")

res = my_int // 2  # // целая часть от деления
print(type(res))
print(res)
print("\u2028")

res = my_int % 3  # % остаток от деления
print(type(res))
print(res)

print("\u2028")
print("\u2028")
print("\u2028")

my_float = 1.2
print(my_float)
print(type(my_float))

print("\u2028")

res = my_int + my_float
print(res)
print(type(res))
print('\u2028')

res = my_int * my_float
print(res)
print(type(res))
print("\u2028")

res = my_int
my_float
print(res)
print(type(res))

print("\u2028")

res = my_int % my_float
print(res)
print(type(res))

print("\u2028")

res = my_int - my_float
print(res)
print(type(res))

print("\u2028")

res = 17 % 4
print(res)

# my_int = 10000  10000
#
# print(my_int)


my_float = 1.2 * (10 ** 300)
print(my_float)

my_float = 0.1 + 0.2
print(my_float)
print((0.1 + 0.2) == 0.3)

my_float = float(12345)
print(my_float)

my_int = int(12345.555)
print(my_int)

# VCS  Version Control  System